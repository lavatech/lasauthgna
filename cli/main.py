# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import getopt
import getpass
import sys
import os
from typing import List, Optional, Dict
from pathlib import Path
from dataclasses import dataclass

import aiohttp

data_dir = (
    Path(os.getenv("XDG_DATA_HOME") or Path.home() / ".local" / "share") / "lasauthgna"
)

# the state of the cli is encoded on those three files.
# a future iteration of this would likely work by using a single "state" file

JID_PATH = data_dir / "jid"
HOST_PATH = data_dir / "host"
TOKEN_PATH = data_dir / "token"

# used for well-known/xmpp-auth.json service discovery
LASAUTHGNA_REL_ID = "urn:xmpp-auth:lasauthgna"


def usage():
    path = Path(sys.argv[0])
    print(f"usage: {path.name} [--force-http] [command]")
    print("\toptions:")
    print(
        "\t\t--force-http - force http when discovering auth server from jid. only use when testing"
    )
    print("logging in:")
    print("\tlauth login jid@host -> give available auth methods for given jid")
    print("\tlauth login jid@host <method> -> attempt to login with given method")
    print(
        "\tlauth login finish jid@host <code> -> finish login with code given via auth method"
    )
    print("\nlinking an email to xmpp account (requires login)")
    print(
        "\tlauth email link <email> -> attempt to update the email linked with your xmpp account"
    )
    print("\tlauth email link finish <code> -> finish email update confirmation")
    print("\nupdate password (requires login)")
    print("\tlauth password change -> change your xmpp password. asks for new password")
    print("\nreset password (no login needed)")
    print(
        "\tlauth password reset <email> -> attempt to reset password for an already linked email"
    )
    print(
        "\tlauth password reset finish <code> -> finish password reset. asks for new password"
    )
    print("\nlogout")
    print("\tlauth logout -> log out of current session")


@dataclass
class CommandContext:
    opts: Dict[str, str]
    argv: List[str]
    session: aiohttp.ClientSession

    async def close(self):
        await self.session.close()

    @property
    def token(self) -> Optional[str]:
        try:
            with open(TOKEN_PATH, "r") as f:
                return f.read()
        except FileNotFoundError:
            return None

    @property
    def host(self) -> Optional[str]:
        try:
            with open(HOST_PATH, "r") as f:
                url = f.read()

                if url.startswith("http://") and ("force-http" not in self.opts):
                    print("WARN: auth server is http, SHOULD be https.")
                    print("\t(disable warning with --force-http)")

                return url
        except FileNotFoundError:
            print("Host file not found. Did you login?")
            return None

    @property
    def jid(self) -> Optional[str]:
        try:
            with open(JID_PATH, "r") as f:
                return f.read()
        except FileNotFoundError:
            return None

    async def ensure_host(self, jid: str) -> None:
        """Ensure that we know the lasauthgna url for a given JID.

        This uses /.well-known/xmpp-auth.json to discover the proper url.
        """

        # Next iteraions could handle multiple JIDs. you need to logout first
        # (which just means deleting all state files) if you want to login
        # to a different xmpp host.
        try:
            f = open(HOST_PATH, "r")
            f.close()
            return
        except FileNotFoundError:
            _user, xmpp_host = jid.split("@")
            host = await fetch_lauth_url(self, xmpp_host)
            with open(HOST_PATH, "w") as f:
                f.write(host)


async def fetch_lauth_url(ctx, xmpp_host):
    prefix = "https" if not "force-http" in ctx.opts else "http"
    async with ctx.session.get(
        f"{prefix}://{xmpp_host}/.well-known/xmpp-auth.json"
    ) as resp:
        if resp.status != 200:
            print(
                f"ERROR: given host, {xmpp_host!r}, does not provide xmpp-auth.json (expected 200, got {resp.status})"
            )
            print("\tif you are sure the xmpp host has lasauthgna, contact its admins")
            sys.exit(1)

        rjson = await resp.json()
        wanted_link = next(
            link for link in rjson["links"] if link["rel"] == LASAUTHGNA_REL_ID
        )
        return wanted_link["href"]


async def init_login(ctx, jid):
    if ctx.jid is not None:
        print("warning: you are likely logged in, please log out")

    await ctx.ensure_host(jid)

    async with ctx.session.post(
        f"{ctx.host}/api/auth/login", json={"jid": jid}
    ) as resp:
        rjson = await resp.json()

        if resp.status != 200:
            print(f"error: {rjson['message']}")
            return

        rjson = await resp.json()
        print("available auth methods: ")
        methods = rjson["authentication_methods"]
        for method in methods:
            if not methods[method]:
                continue

            print(f"\t- {method}")


async def start_login(ctx, jid: str, auth_method: str):
    if ctx.jid is not None:
        print("warning: you are likely logged in, please log out")

    await ctx.ensure_host(jid)
    async with ctx.session.post(
        f"{ctx.host}/api/auth/login/start",
        json={"jid": jid, "auth_method": auth_method},
    ) as resp:
        if resp.status not in (200, 204):
            rjson = await resp.json()
            print(f"error: {rjson['message']}")
            return

        print("ok! please check in with your selected auth method.")


async def finish_login(ctx, jid: str, token: str):
    await ctx.ensure_host(jid)
    async with ctx.session.post(
        f"{ctx.host}/api/auth/login/finish", json={"jid": jid, "token": token}
    ) as resp:
        rjson = await resp.json()

        if resp.status != 200:
            print(f"error: {rjson['message']}")
            return

        jid = rjson["user"]["jid"]
        email = rjson["user"]["email"]
        print(f"logged in as {jid} (email {email})")

        with open(TOKEN_PATH, "w") as f:
            # actual bearer token comes in rjson, and not the token we give.
            # the token we give is of type 'login', but we need a 'bearer'.
            f.write(rjson["token"])


async def login(ctx):
    subcommand = ctx.argv[2]
    if subcommand == "finish":
        await finish_login(ctx, ctx.argv[3], ctx.argv[4])
    else:
        # subcommand is a jid
        jid = subcommand

        # next arg may be the auth method, and if it is, we need to
        # start a login
        try:
            auth_method = ctx.argv[3]
            await start_login(ctx, jid, auth_method)
        except IndexError:
            await init_login(ctx, jid)


async def email_link_start(ctx, email: str):
    async with ctx.session.patch(
        f"{ctx.host}/api/profile/email",
        json={"email": email},
        headers={"Authorization": f"Bearer {ctx.token}"},
    ) as resp:
        rjson = await resp.json()
        if resp.status != 200:
            print(f"error: {rjson['message']}")
            return

        print("ok! please confirm email sent to you")

        # TODO: use sys.argv[0] instead of lauth
        print("finish confirmation with 'lauth email link finish <code>'")


async def email_link_finish(ctx, token: str):
    async with ctx.session.post(
        f"{ctx.host}/api/profile/email/finish",
        json={"token": token},
        headers={"Authorization": f"Bearer {ctx.token}"},
    ) as resp:
        rjson = await resp.json()
        if resp.status != 200:
            print(f"error: {rjson['message']}")
            return

        print(f"ok! your email is now set to {rjson['email']}")


async def email(ctx):
    subcommand = ctx.argv[2]
    if subcommand == "link":
        maybe_email = ctx.argv[3]
        if maybe_email == "finish":
            await email_link_finish(ctx, ctx.argv[4])
        else:
            await email_link_start(ctx, maybe_email)


async def password_change(ctx):
    new_password = fetch_password()
    if new_password is None:
        print("no confirmed password. exiting")
        return

    async with ctx.session.post(
        f"{ctx.host}/api/auth/change_password",
        json={"password": new_password},
        headers={"Authorization": f"Bearer {ctx.token}"},
    ) as resp:
        if resp.status != 204:
            rjson = await resp.json()
            print(f"error: {rjson['message']}")
            return

        print("ok! your xmpp account's password was changed")


async def password_reset_start(ctx, email: str):
    async with ctx.session.post(
        f"{ctx.host}/api/auth/reset_password",
        json={"email": email},
    ) as resp:
        rjson = await resp.json()
        if resp.status != 200:
            print(f"error: {rjson['message']}")
            return

        print("ok! please confirm email sent to you")

        # TODO: use sys.argv[0] instead of lauth
        print(
            "finish password reset with 'lauth password reset finish <code> <new_password>'"
        )


def fetch_password():
    while True:
        new_password = getpass.getpass("please type your new password: ")
        print(f"got {len(new_password)} characters")

        confirm = input("confirm password change (y/n/q)? ")
        if confirm == "y":
            return new_password
        elif confirm == "q":
            return None


async def password_reset_finish(ctx, token: str):
    new_password = fetch_password()
    if new_password is None:
        print("no confirmed password. exiting")
        return

    async with ctx.session.post(
        f"{ctx.host}/api/auth/reset_password/finish",
        json={"token": token, "password": new_password},
    ) as resp:
        if resp.status != 204:
            rjson = await resp.json()
            print(f"error: {rjson['message']}")
            return

        print(f"ok! your xmpp password has been reset")


async def password(ctx):
    subcommand = ctx.argv[2]
    if subcommand == "change":
        await password_change(ctx)
    elif subcommand == "reset":
        maybe_email = ctx.argv[3]
        if maybe_email == "finish":
            await password_reset_finish(ctx, ctx.argv[4])
        else:
            await password_reset_start(ctx, maybe_email)


async def logout(ctx):
    try:
        HOST_PATH.unlink()
    except FileNotFoundError:
        pass

    try:
        TOKEN_PATH.unlink()
    except FileNotFoundError:
        pass

    try:
        JID_PATH.unlink()
    except FileNotFoundError:
        pass

    print("ok")


async def main_with_args(argv: List[str]):
    # ensure data_dir exists
    data_dir.mkdir(exist_ok=True)

    # it doesn't work without removing the 0th element
    opts, args = getopt.getopt(argv[1:], "", ["force-http"])

    # proposed cli:

    # login flow:
    #  lauth login jid@a3.pm -> gives methods
    #  lauth login <jid> <method>
    #  lauth login finish <code>

    # link email flow:
    #  lauth email link <email>
    #  lauth email link finish <code>

    # change password flow:
    #  lauth password change (only when logged in)

    # reset password flow:
    #  lauth password reset <email> (dont need login)
    #  lauth password reset finish <code> (dont need login)

    # re-create args with argv[0] at the start of it
    args = [sys.argv[0]] + args
    if len(args) == 1:
        usage()
        return 0

    command = args[1]
    # turn it into a dict
    optsdict = {key.lstrip("-"): value for key, value in opts}
    ctx = CommandContext(optsdict, args, aiohttp.ClientSession())
    if command == "login":
        await login(ctx)
    elif command == "email":
        await email(ctx)
    elif command == "password":
        await password(ctx)
    elif command == "logout":
        await logout(ctx)
    else:
        usage()
        return 0

    await ctx.close()
    return 0


def main():
    loop = asyncio.get_event_loop()
    retvalue = loop.run_until_complete(main_with_args(sys.argv))
    sys.exit(retvalue)
