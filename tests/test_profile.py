# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only
import re
import pytest

from lasauthgna.model import User
from tests.util.generators import email
from lasauthgna.emailer import EmailQueue
from tests.util.mock import TestEmailState

pytestmark = pytest.mark.asyncio

EMAIL_REGEX = re.compile(r"token: '(.*)'")


async def test_email_patch_flow(client):
    new_email = email()

    resp = await client.patch("/api/profile/email", json={"email": new_email})
    assert resp.status_code == 200

    rjson = await resp.json
    job_id = rjson["job_id"]
    await EmailQueue.wait_job(job_id, timeout=10)

    # assert there's one email
    emails = TestEmailState.emails
    assert emails

    to_address, _subject, body = emails[-1]
    assert to_address == new_email

    match = EMAIL_REGEX.search(body)
    assert match
    token = match.group(1)
    assert token

    resp = await client.post("/api/profile/email/finish", json={"token": token})
    assert resp.status_code == 200

    async with client.app.app_context():
        client.user = await User.fetch(client.user.jid)

    assert client.user.email == new_email
