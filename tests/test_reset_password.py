# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only
import re
import pytest

from tests.util.generators import username
from lasauthgna.emailer import EmailQueue
from lasauthgna.reset_pass import ResetPasswordQueue
from tests.util.mock import TestEmailState

pytestmark = pytest.mark.asyncio

EMAIL_REGEX = re.compile(r"token: '(.*)'")


async def test_change_password(client):
    new_password = username()

    resp = await client.post(
        "/api/auth/reset_password", json={"email": client.user.email}
    )
    assert resp.status_code == 200

    rjson = await resp.json

    reset_job_id = rjson["job_id"]
    await ResetPasswordQueue.wait_job(reset_job_id, timeout=10)

    # get the emailer job id from the password reset job id
    emailer_job_id = await ResetPasswordQueue.fetch_job_state(reset_job_id)

    # as the test user already exists, the job will always succeed
    assert emailer_job_id is not None

    await EmailQueue.wait_job(emailer_job_id, timeout=10)

    # assert there's one email
    emails = TestEmailState.emails
    assert emails

    to_address, _subject, body = emails[-1]
    assert to_address == client.user.email

    match = EMAIL_REGEX.search(body)
    assert match
    token = match.group(1)
    assert token

    resp = await client.post(
        "/api/auth/reset_password/finish",
        json={"token": token, "password": new_password},
    )
    assert resp.status_code == 204

    # check the mock ejabberd if it has the wanted password
    assert client.app.ejabberd.jids[client.user.jid] == new_password
