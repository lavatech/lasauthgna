# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import os
import sys

import pytest

sys.path.append(os.getcwd())
from lasauthgna import app as app_  # noqa: E402
from lasauthgna.model import User, Token  # noqa: E402
from lasauthgna.enums import TokenType  # noqa: E402
from tests.util.generators import jid, email  # noqa: E402
from tests.util.client import TestClient  # noqa: E402
from tests.util.mock import EjabberdMock  # noqa: E402

# from tests.util.generators import hexs, username, email  # noqa: E402


@pytest.yield_fixture(name="event_loop", scope="session")
def event_loop_fixture():
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(name="app", scope="session")
async def app_fixture(event_loop):
    app_.loop = event_loop
    async with app_.app_context():
        await app_.startup()
    app_.ejabberd = EjabberdMock()
    yield app_
    async with app_.app_context():
        await app_.shutdown()


@pytest.fixture(name="raw_test_client")
def test_cli_fixture(app):
    return app.test_client()


@pytest.fixture(name="test_user")
async def test_user_fixture(app):
    test_jid = jid()
    test_email = email()

    async with app.app_context():
        user = await User.create(test_jid, email=test_email)
        app.ejabberd.add_user(user.jid)

    yield user

    async with app.app_context():
        await user.delete()
        app.ejabberd.remove_user(user.jid)


@pytest.fixture(name="client")
async def test_client_fixture(raw_test_client, test_user):
    """Yield a TestClient instance that contains a randomly generated user."""
    async with raw_test_client.app.app_context():
        bearer_token = await Token.create(test_user.jid, TokenType.Bearer)
    yield TestClient(raw_test_client, test_user, bearer_token)
