# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import secrets
import random

__all__ = [
    "token",
    "hexs",
    "username",
    "email",
    "jid",
]


def token() -> str:
    return secrets.token_urlsafe(random.randint(100, 300))


def hexs(len: int = 5) -> str:
    return secrets.token_hex(len)


def username() -> str:
    return hexs(6)


def email() -> str:
    name = hexs()
    return f"{name}@discordapp.io"


def jid() -> str:
    return f"{hexs()}@{hexs()}.io"
