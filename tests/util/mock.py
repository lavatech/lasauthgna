# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
from typing import Any, List, Tuple, Dict

from expiringdict import ExpiringDict

from tests.util.generators import username

import lasauthgna.emailer

log = logging.getLogger(__name__)


class ResponseWrapper:
    def __init__(self, data: Any, status: int = 200):
        self.status = status
        self.data = data

    async def json(self):
        return self.data

    async def __aenter__(self):
        return self

    async def __aexit__(self, _a, _b, _c):
        pass


class EjabberdMock:
    def __init__(self):
        self.unknown_jid_cache = ExpiringDict(max_len=1024, max_age_seconds=100)
        self.messages = []

        # map jid to password
        self.jids: Dict[str, str] = {}

    async def init(self):
        pass

    def add_user(self, jid: str):
        log.info("Added %r to ejabberd mock list", jid)
        self.jids[jid] = username()

    def remove_user(self, jid: str):
        log.info("Removed %r to ejabberd mock list", jid)
        self.jids.pop(jid)

    def send_message(self, jid_to: str, body: str):
        if jid_to not in self.jids:
            log.info("jid %r not found", jid_to)
            return ResponseWrapper(None, 400)

        log.info("Sent %r to mock %r", body, jid_to)
        self.messages.append({"jid": jid_to, "body": body})
        return ResponseWrapper(0)

    def check_account(self, jid: str):
        return ResponseWrapper(0 if jid in self.jids else 1)

    def change_password(self, jid: str, password: str):
        try:
            self.jids[jid] = password
            return ResponseWrapper(0)
        except KeyError:
            return ResponseWrapper(1)


class TestEmailState:
    emails: List[Tuple[str, str, str]] = []


def new_raw_send(_smtp, _cfg, to, subject, content):
    log.info("Sent mock email to %r", to)
    TestEmailState.emails.append((to, subject, content))


lasauthgna.emailer.raw_send_email = new_raw_send
lasauthgna.emailer.raw_connect_smtp = lambda cfg: 1
