# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only
import pytest

from tests.util.generators import username

pytestmark = pytest.mark.asyncio


async def test_change_password(client):
    new_password = username()
    resp = await client.post(
        "/api/auth/change_password", json={"password": new_password}
    )
    assert resp.status_code == 204
