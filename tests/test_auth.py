# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only
import re
import pytest

from lasauthgna.model import User
from tests.util.generators import jid
from lasauthgna.emailer import EmailQueue
from tests.util.mock import TestEmailState

pytestmark = pytest.mark.asyncio

EMAIL_REGEX = re.compile(r"token: '(.*)'")


async def test_invalid_jid(raw_test_client):
    client = raw_test_client
    test_jid = jid()

    resp = await client.post("/api/auth/login", json={"jid": test_jid})
    assert resp.status_code == 401
    rjson = await resp.json
    assert rjson["error"]
    assert rjson["message"]

    # ensure that we used the cache by doing the same request again
    resp = await client.post("/api/auth/login", json={"jid": test_jid})
    assert resp.status_code == 401
    rjson = await resp.json
    assert rjson["error"]
    assert rjson["message"]


async def test_new_jid(raw_test_client):
    client = raw_test_client
    test_jid = jid()

    raw_test_client.app.ejabberd.add_user(test_jid)

    try:
        resp = await client.post("/api/auth/login", json={"jid": test_jid})
        assert resp.status_code == 200
    finally:
        # ensure we delete the test jid from our records
        raw_test_client.app.ejabberd.remove_user(test_jid)
        async with raw_test_client.app.app_context():
            user = await User.fetch(test_jid)
            await user.delete()


async def test_start_invalid_jid(client):
    resp = await client.post(
        "/api/auth/login/start", json={"jid": jid(), "auth_method": "xmpp"}
    )
    assert resp.status_code not in (200, 204)
    rjson = await resp.json
    assert rjson["error"]
    assert rjson["message"]


async def test_login_flow_invalid_token(client):
    resp = await client.post(
        "/api/auth/login/start", json={"jid": client.user.jid, "auth_method": "xmpp"}
    )
    assert resp.status_code == 204

    resp = await client.post(
        "/api/auth/login/finish", json={"jid": client.user.jid, "token": jid()}
    )
    assert resp.status_code not in (200, 204)
    rjson = await resp.json
    assert rjson["error"]
    assert rjson["message"]


async def test_login_flow(client):
    test_jid = client.user.jid

    resp = await client.post("/api/auth/login", json={"jid": test_jid})
    assert resp.status_code == 200
    rjson = await resp.json
    assert isinstance(rjson, dict)
    methods = rjson["authentication_methods"]
    assert isinstance(methods, dict)
    assert isinstance(methods["xmpp"], bool)
    assert isinstance(methods["email"], bool)

    assert methods["xmpp"]
    assert methods["email"]

    resp = await client.post(
        "/api/auth/login/start", json={"jid": test_jid, "auth_method": "xmpp"}
    )
    assert resp.status_code == 204

    message = client.app.ejabberd.messages[-1]
    assert message["jid"] == test_jid
    _, token = message["body"].split(":")
    token = token.strip()

    resp = await client.post(
        "/api/auth/login/finish", json={"jid": test_jid, "token": token}
    )
    assert resp.status_code == 200

    rjson = await resp.json
    assert rjson["user"]["jid"] == test_jid


async def test_login_flow_with_email(client):
    test_jid = client.user.jid

    resp = await client.post("/api/auth/login", json={"jid": test_jid})
    assert resp.status_code == 200
    rjson = await resp.json
    methods = rjson["authentication_methods"]
    assert methods["email"]

    resp = await client.post(
        "/api/auth/login/start", json={"jid": test_jid, "auth_method": "email"}
    )
    assert resp.status_code == 200
    rjson = await resp.json
    await EmailQueue.wait_job(rjson["job_id"], timeout=10)

    # assert there's one email
    emails = TestEmailState.emails
    assert emails

    to_address, _subject, body = emails[-1]
    assert to_address == client.user.email

    match = EMAIL_REGEX.search(body)
    assert match
    token = match.group(1)
    assert token

    resp = await client.post(
        "/api/auth/login/finish", json={"jid": test_jid, "token": token}
    )
    assert resp.status_code == 200

    rjson = await resp.json
    assert rjson["user"]["jid"] == test_jid
