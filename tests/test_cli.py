# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import pytest

from cli.main import main_with_args

pytestmark = pytest.mark.asyncio


# We only test the help command because testing the full API through the
# cli tool is complicated, especially when we need to implement /.well-known/
# bits on a test ejabberd instance (which we can't just spawn in the
# test suite)
#
# Manual testing will have to suffice.


async def _run(args):
    return await main_with_args(["--force-http"] + args)


async def test_help():
    status = await _run([""])
    assert status == 0
