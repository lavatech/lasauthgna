-- START LASAUTHGNA SCHEMA

CREATE TABLE IF NOT EXISTS lasauthgna_users (
    jid TEXT PRIMARY KEY,

    -- can be NULL
    email TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS lasauthgna_tokens (
    jid TEXT NOT NULL REFERENCES lasauthgna_users ON DELETE CASCADE,

    value TEXT PRIMARY KEY NOT NULL,
    type TEXT NOT NULL,

    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc'),
    expires_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc')
);

CREATE TABLE IF NOT EXISTS lasauthgna_email_tokens (
    token TEXT PRIMARY KEY REFERENCES lasauthgna_tokens (value) ON DELETE CASCADE,
    new_email TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS lasauthgna_email_queue (
    job_id UUID PRIMARY KEY,
    name TEXT UNIQUE,

    state BIGINT DEFAULT 0,
    errors TEXT DEFAULT '',
    inserted_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() AT TIME ZONE 'utc'),
    scheduled_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() AT TIME ZONE 'utc'),
    taken_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
    internal_state JSONB DEFAULT '{}',

    to_address TEXT NOT NULL,
    subject TEXT NOT NULL,
    body TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS lasauthgna_password_reset_queue (
    job_id UUID PRIMARY KEY,
    name TEXT UNIQUE,

    state BIGINT DEFAULT 0,
    errors TEXT DEFAULT '',
    inserted_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() AT TIME ZONE 'utc'),
    scheduled_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() AT TIME ZONE 'utc'),
    taken_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
    internal_state JSONB DEFAULT '{}',

    email TEXT NOT NULL
);

-- END LASAUTHGNA SCHEMA
