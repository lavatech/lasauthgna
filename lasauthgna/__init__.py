# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import logging

import asyncpg
from tomlkit import parse
import quart
from quart import Quart

from violet import JobManager

from lasauthgna.bp import index, cors, auth, profile, password
from lasauthgna.util import toml_to_native
from lasauthgna.errors import APIError
from lasauthgna.ejabberd import EjabberdClient
from lasauthgna.emailer import EmailQueue
from lasauthgna.reset_pass import ResetPasswordQueue

log = logging.getLogger(__name__)
ROOT_PREFIXES = ("/api", "/api/master")


def create_app() -> Quart:
    app = Quart("lasauthgna")

    with open("config.toml", "r") as config_file:
        app.cfg = parse(config_file.read())

    log_defaults = {"format": "%(asctime)s - %(levelname)s [%(name)s] %(message)s"}
    cfg_log = app.cfg.get("log", {})
    merged = {**cfg_log, **log_defaults}

    logging.basicConfig(**merged)
    return app


def setup_blueprints(app_: Quart) -> None:

    # use None to load the blueprint under /
    # use an empty string to load the blueprint under /api
    # use a non-empty string to load the blueprint under /api<your string>
    blueprint_list = [
        (index, None),
        (cors, ""),
        (auth, "/auth"),
        (password, "/auth"),
        (profile, "/profile"),
    ]

    for blueprint, api_prefix in blueprint_list:
        route_prefixes = [f'{root}{api_prefix or ""}' for root in ROOT_PREFIXES]

        if api_prefix is None:
            route_prefixes = [""]

        log.debug(
            "loading blueprint %r with prefixes %r", blueprint.name, route_prefixes
        )
        for route in route_prefixes:
            app_.register_blueprint(blueprint, url_prefix=route)


app = create_app()


@app.before_serving
async def app_before_serving():
    try:
        app.loop
    except AttributeError:
        app.loop = asyncio.get_event_loop()

    log.info("connecting to db")
    app.db = await asyncpg.create_pool(**toml_to_native(app.cfg["database"]))

    log.info("start ejabberd manager")
    app.ejabberd = EjabberdClient()
    await app.ejabberd.init()

    log.info("start job manager")
    app.sched = JobManager(db=app.db, context_function=app.app_context)
    app.sched.register_job_queue(EmailQueue)
    app.sched.register_job_queue(ResetPasswordQueue)


@app.after_serving
async def app_after_serving():
    try:
        await app.sched.stop_all(wait=True, timeout=10)
    except asyncio.TimeoutError:
        log.warning("timed out waiting for tasks. ignoring and closing")

    log.info("closing db")
    await app.db.close()


@app.errorhandler(APIError)
async def handle_api_error(exc):
    log.warning("API error: %r", exc)
    body = {"error": True, "message": exc.message}
    body.update(exc.payload_body)
    return body, exc.status_code


@app.errorhandler(Exception)
async def handle_any_error(exc):
    if isinstance(exc, quart.exceptions.HTTPException):
        return exc.description, exc.status_code
    log.exception("An error occoured %r", exc)
    body = {"error": True, "message": repr(exc)}
    return body, 500


# NOTE: handlers go here such as before_serving / after_serving

setup_blueprints(app)
