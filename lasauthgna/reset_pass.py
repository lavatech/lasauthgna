# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

from typing import Tuple

from violet import JobQueue
from violet.fail_modes import Retry

from .model import Token, User
from .enums import TokenType
from .emailer import EmailQueue


class ResetPasswordQueue(JobQueue):
    name = "lasauthgna_password_reset_queue"
    args = ("email",)
    workers = 1
    fail_mode = Retry(retry_max_attempts=10)

    @classmethod
    def map_persisted_row(cls, row) -> Tuple[str]:
        return (row["email"],)

    @classmethod
    async def submit(cls, email: str, **kwargs):
        return await cls._sched.raw_push(cls, (email,), **kwargs)

    @classmethod
    async def handle(cls, ctx):
        email = ctx.args[0]
        user = await User.fetch_by(email=email)
        if user is None:
            await cls.set_job_state(ctx.job_id, None)
            return

        reset_token = await Token.create(user.jid, TokenType.ResetPassword)
        job_id = await EmailQueue.submit(
            email,
            "awoo!!! - reset password",
            f"here's the token: '{reset_token.value}'\nTODO: url maybe?",
        )

        await cls.set_job_state(ctx.job_id, str(job_id))
