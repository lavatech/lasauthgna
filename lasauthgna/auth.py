# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

from quart import request
from .model import Token, User
from .enums import TokenType
from .errors import Unauthorized


async def fetch_auth(token_type: TokenType) -> User:
    # login tokens should never be used for API access beyond
    # authentication
    assert token_type != TokenType.Login

    header_value = request.headers["Authorization"]
    token_value = header_value[len("Bearer ") :]

    token = await Token.fetch(token_type, token_value)
    if token is None:
        raise Unauthorized(f"Invalid token (got {token_value!r})")

    user = await User.fetch(token.jid)
    assert user is not None
    return user
