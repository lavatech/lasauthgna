# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import aiohttp
from quart import current_app as app
from expiringdict import ExpiringDict


class EjabberdClient:
    def __init__(self):
        self.cfg = app.cfg["ejabberd"]
        self.api_url = self.cfg["api_url"]
        self.unknown_jid_cache = ExpiringDict(max_len=1024, max_age_seconds=100)

    async def init(self):
        auth = aiohttp.BasicAuth(self.cfg["user"], self.cfg["password"])
        self.session = aiohttp.ClientSession(auth=auth)

    def send_message(self, jid_to: str, body: str):
        return self.session.post(
            f"{self.api_url}/send_message",
            json={
                "from": self.cfg["from"],
                "to": jid_to,
                "type": "chat",
                "subject": "",
                "body": body,
            },
        )

    def check_account(self, jid: str):
        user, host = jid.split("@")
        return self.session.post(
            f"{self.api_url}/check_account", json={"user": user, "host": host}
        )

    def change_password(self, jid: str, new_password: str):
        user, host = jid.split("@")
        return self.session.post(
            f"{self.api_url}/change_password",
            json={"user": user, "host": host, "newpass": new_password},
        )
