# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import re
from typing import Any
from cerberus import Validator
from .errors import BadRequest
from .enums import AuthMethod

EMAIL_REGEX = re.compile(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", re.A)


class CustomValidator(Validator):
    def _validate_type_jid(self, value: str):
        # I think that "jid" (not including resource) being the same thing as email
        # is a good thing.
        #
        # we can't just split('@') and just validate the username either, as
        # the host is sent to ejabberd as well
        return bool(EMAIL_REGEX.match(value))

    def _validate_type_auth_method(self, value: str):
        return value in ("email", "xmpp")

    def _validate_type_email(self, value: str) -> bool:
        return bool(EMAIL_REGEX.match(value))

    def _validate_type_password(self, value: str) -> bool:
        # there are no double-digit restrictions due to argon2 being better
        # than bcrypt (lol)
        #
        # regardless, we keep a limit of 8KB to not cause uneeded overload.
        return len(value) > 8 and len(value) < 8192


def validate(document: Any, schema: Any) -> Any:
    """Validate the given document against a given schema."""
    if document is None:
        raise BadRequest("Missing document. Must be JSON")

    validator = CustomValidator(schema)
    validator.allow_unknown = False

    if not validator.validate(document):
        raise BadRequest("Bad payload", validator.errors)

    return validator.document


LOGIN_SCHEMA = {"jid": {"type": "jid", "required": True}}

LOGIN_START_SCHEMA = {
    **LOGIN_SCHEMA,
    **{"auth_method": {"coerce": lambda x: AuthMethod(x), "required": True}},
}

LOGIN_FINISH_SCHEMA = {
    **LOGIN_SCHEMA,
    **{"token": {"type": "string", "minlength": 20, "maxlength": 256}},
}

PATCH_EMAIL_SCHEMA = {
    "email": {"type": "email", "required": True},
}

FINISH_PATCH_EMAIL_SCHEMA = {
    "token": {"type": "string", "required": True},
}

CHANGE_PASSWORD_SCHEMA = {
    "password": {"type": "password", "required": True},
}

RESET_PASSWORD_START_SCHEMA = PATCH_EMAIL_SCHEMA
RESET_PASSWORD_FINISH_SCHEMA = {
    "token": {"type": "string", "required": True},
    "password": {"type": "password", "required": True},
}
