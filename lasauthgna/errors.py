# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only


class APIError(Exception):
    status_code = 500

    @property
    def message(self) -> str:
        try:
            return self.args[0]
        except IndexError:
            return repr(self)

    @property
    def payload(self) -> dict:
        try:
            return self.args[1]
        except IndexError:
            return {}

    @property
    def payload_body(self) -> dict:
        return self.payload


class BadRequest(APIError):
    status_code = 400

    @property
    def payload_body(self) -> dict:
        return {"validation_errors": self.payload}


class Unauthorized(APIError):
    status_code = 401


class Forbidden(APIError):
    status_code = 403


class FeatureDisabled(APIError):
    status_code = 405
