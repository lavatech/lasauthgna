# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

from typing import Optional
from quart import current_app as app
from asyncpg import Record


class User:
    def __init__(self, record: Record):
        self.jid: str = record["jid"]
        self.email: Optional[str] = record["email"]

    @classmethod
    async def fetch(_cls, jid: str) -> Optional["User"]:
        record = await app.db.fetchrow(
            """
            SELECT jid, email
            FROM lasauthgna_users
            WHERE jid = $1
            """,
            jid,
        )

        if record is None:
            return None

        return User(record)

    @classmethod
    async def fetch_by(_cls, *, email: str) -> Optional["User"]:
        record = await app.db.fetchrow(
            """
            SELECT jid, email
            FROM lasauthgna_users
            WHERE email = $1
            """,
            email,
        )

        if record is None:
            return None

        return User(record)

    @classmethod
    async def create(_cls, jid: str, *, email: Optional[str] = None) -> "User":
        record = await app.db.fetchrow(
            """
            INSERT INTO lasauthgna_users (jid, email)
            VALUES ($1, $2)
            RETURNING jid, email
            """,
            jid,
            email,
        )
        assert record is not None
        return User(record)

    async def delete(self) -> None:
        await app.db.execute(
            """
            DELETE FROM lasauthgna_users
            WHERE jid = $1
            """,
            self.jid,
        )

    def to_dict(self) -> dict:
        return {"jid": self.jid, "email": self.email}

    async def set_email(self, new_email: Optional[str]) -> None:
        # TODO a generic update function, maybe?
        await app.db.execute(
            """
            UPDATE lasauthgna_users
            SET email = $1
            WHERE jid = $2
            """,
            new_email,
            self.jid,
        )
        self.email = new_email
