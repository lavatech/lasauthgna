# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import secrets
from datetime import datetime, timedelta
from typing import Optional
from asyncpg import Record
from quart import current_app as app
from ..enums import TokenType


def fetch_expiration(type: TokenType, *, now: Optional[datetime] = None) -> datetime:
    now = now or datetime.utcnow()

    # TODO finish determining expiry for other token types

    if type == TokenType.Login:
        delta = timedelta(minutes=10)
    elif type == TokenType.Bearer:
        delta = timedelta(minutes=15)
    elif type == TokenType.EmailLink:
        delta = timedelta(minutes=15)
    elif type == TokenType.ResetPassword:
        delta = timedelta(minutes=10)

    return now + delta


class Token:
    def __init__(self, record: Record):
        self.jid: str = record["jid"]
        self.type = TokenType(record["type"])
        self.value: str = record["value"]
        self.created_at: datetime = record["created_at"]
        self.expires_at: datetime = record["expires_at"]

    @classmethod
    async def fetch(_cls, type: TokenType, token: str) -> Optional["Token"]:
        """Attempt to fetch a given token.

        If the token is expired, deletes it from the database and returns None.
        """
        record = await app.db.fetchrow(
            """
            SELECT jid, type, value, created_at, expires_at
            FROM lasauthgna_tokens
            WHERE type = $1 AND value = $2
            """,
            type.value,
            token,
        )

        if record is None:
            return None

        utcnow = datetime.utcnow()
        if utcnow > record["expires_at"]:
            await (Token(record)).delete()
            return None

        return Token(record)

    @classmethod
    async def create(
        _cls, jid: str, type: TokenType, *, expires_at: Optional[datetime] = None
    ) -> "Token":
        """Generate a token."""
        token = secrets.token_urlsafe(32)

        created_at = datetime.utcnow()
        expires_at = expires_at or fetch_expiration(type, now=created_at)

        record = await app.db.fetchrow(
            """
            INSERT INTO lasauthgna_tokens (jid, type, value, created_at, expires_at)
            VALUES ($1, $2, $3, $4, $5)
            RETURNING jid, type, value, created_at, expires_at
            """,
            jid,
            type.value,
            token,
            created_at,
            expires_at,
        )

        assert record is not None

        return Token(record)

    async def delete(self) -> None:
        """Delete a token."""
        await app.db.execute(
            """
            DELETE FROM lasauthgna_tokens
            WHERE type = $1 AND value = $2
            """,
            self.type.value,
            self.value,
        )
