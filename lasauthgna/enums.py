# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

from enum import Enum


class AuthMethod(Enum):
    xmpp = "xmpp"
    email = "email"


class TokenType(Enum):
    Login = "login"
    Bearer = "bearer"
    EmailLink = "email_link"
    ResetPassword = "reset_password"
