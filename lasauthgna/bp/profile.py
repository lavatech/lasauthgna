# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
from quart import Blueprint, request, jsonify, current_app as app
from ..errors import Unauthorized
from ..auth import fetch_auth
from ..enums import TokenType
from ..schema import validate, PATCH_EMAIL_SCHEMA, FINISH_PATCH_EMAIL_SCHEMA
from ..emailer import EmailQueue
from ..model import Token, User

log = logging.getLogger(__name__)
bp = Blueprint("profile", __name__)


@bp.route("/email", methods=["PATCH"])
async def start_update_email():
    user = await fetch_auth(TokenType.Bearer)

    j = validate(await request.get_json(), PATCH_EMAIL_SCHEMA)
    email = j["email"]

    update_token = await Token.create(user.jid, TokenType.EmailLink)
    await app.db.execute(
        """
        INSERT INTO lasauthgna_email_tokens
            (token, new_email)
        VALUES
            ($1, $2)
        """,
        update_token.value,
        email,
    )

    job_id = await EmailQueue.submit(
        email, "awoo!!!", f"here's the token: '{update_token.value}'\nTODO: url maybe?"
    )

    return jsonify({"job_id": str(job_id)})


@bp.route("/email/finish", methods=["PUT", "POST"])
async def finish_update_email():
    j = validate(await request.get_json(), FINISH_PATCH_EMAIL_SCHEMA)
    token = await Token.fetch(TokenType.EmailLink, j["token"])
    if token is None:
        raise Unauthorized("Invalid token")

    user = await User.fetch(token.jid)
    assert user is not None

    new_email = await app.db.fetchval(
        """
        SELECT new_email
        FROM lasauthgna_email_tokens
        WHERE token = $1
        """,
        token.value,
    )
    assert new_email is not None

    await user.set_email(new_email)
    await token.delete()

    return {"email": new_email}
