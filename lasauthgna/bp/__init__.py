# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

from .index import bp as index
from .cors import bp as cors
from .auth import bp as auth
from .profile import bp as profile
from .password import bp as password

__all__ = ["index", "cors", "auth", "profile", "password"]
