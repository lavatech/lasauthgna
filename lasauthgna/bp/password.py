# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
from quart import Blueprint, request, current_app as app, jsonify
from ..auth import fetch_auth

from ..schema import (
    validate,
    CHANGE_PASSWORD_SCHEMA,
    RESET_PASSWORD_START_SCHEMA,
    RESET_PASSWORD_FINISH_SCHEMA,
)
from ..enums import TokenType
from ..model import Token, User
from ..errors import Unauthorized, FeatureDisabled
from ..reset_pass import ResetPasswordQueue

log = logging.getLogger(__name__)
bp = Blueprint("password", __name__)


async def notify_password_change(user: User) -> None:
    async with app.ejabberd.send_message(
        user.jid,
        "Your XMPP password was changed.\nIf this action was not made by you, please contact the instance administrators immediately.",
    ) as resp:
        assert resp.status == 200
        rjson = await resp.json()
        if rjson != 0:
            raise Exception("Failed to send message to XMPP.")


@bp.route("/change_password", methods=["POST"])
async def change_password():
    if not app.cfg["features"]["change_password"]:
        raise FeatureDisabled("Password Changes are currently disabled.")

    user = await fetch_auth(TokenType.Bearer)
    j = validate(await request.get_json(), CHANGE_PASSWORD_SCHEMA)
    new_password = j["password"]

    # attempt to send notification first, actually update password second
    await notify_password_change(user)
    async with app.ejabberd.change_password(user.jid, new_password):
        return "", 204


@bp.route("/reset_password", methods=["POST"])
async def reset_password_start():
    if not app.cfg["features"]["reset_password"]:
        raise FeatureDisabled("Password Resets are currently disabled.")

    j = validate(await request.get_json(), RESET_PASSWORD_START_SCHEMA)
    email = j["email"]

    job_id = await ResetPasswordQueue.submit(email)
    return jsonify({"job_id": str(job_id)})


@bp.route("/reset_password/finish", methods=["POST"])
async def reset_password_finish():
    j = validate(await request.get_json(), RESET_PASSWORD_FINISH_SCHEMA)

    token = await Token.fetch(TokenType.ResetPassword, j["token"])
    if token is None:
        raise Unauthorized("Invalid token")

    user = await User.fetch(token.jid)
    assert user is not None

    # attempt to send notification first, actually update password second
    await notify_password_change(user)
    async with app.ejabberd.change_password(user.jid, j["password"]):
        await token.delete()
        return "", 204
