# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

from quart import Blueprint

bp = Blueprint("index", __name__)


@bp.route("/", methods=["GET"])
async def index_route():
    return "nyanya"
