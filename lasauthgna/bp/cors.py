# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

from quart import Blueprint, current_app as app

bp = Blueprint("cors", __name__)


@bp.after_app_request
async def set_cors_headers(resp):
    """Set CORS headers for response."""
    allowed_origin = app.cfg["cors"]["allowed_origin"]
    resp.headers["Access-Control-Allow-Origin"] = allowed_origin

    resp.headers[
        "Access-Control-Allow-Headers"
    ] = "*, Content-Type, Authorization, Origin"

    resp.headers[
        "Access-Control-Expose-Headers"
    ] = "X-Ratelimit-Limit, X-Ratelimit-Remaining, X-Ratelimit-Reset"

    resp.headers["Access-Control-Allow-Methods"] = resp.headers.get("allow", "*")

    return resp
