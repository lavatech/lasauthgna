# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging
from quart import Blueprint, request, jsonify, current_app as app
from ..schema import validate, LOGIN_SCHEMA, LOGIN_START_SCHEMA, LOGIN_FINISH_SCHEMA
from ..errors import Unauthorized, BadRequest, FeatureDisabled
from ..model import User, Token
from ..enums import AuthMethod, TokenType
from ..emailer import EmailQueue

log = logging.getLogger(__name__)
bp = Blueprint("auth", __name__)


@bp.route("/login", methods=["POST"])
async def init_login():
    if not app.cfg["features"]["login"]:
        raise FeatureDisabled("Logins are currently disabled.")

    j = validate(await request.get_json(), LOGIN_SCHEMA)

    jid = j["jid"]

    try:
        app.ejabberd.unknown_jid_cache[jid]
        log.debug("user %r is unknown in cache", jid)
        raise Unauthorized("Account not found")
    except KeyError:
        pass

    user = await User.fetch(jid)
    if user is not None:
        return jsonify(
            {"authentication_methods": {"xmpp": True, "email": user.email is not None}}
        )

    # we don't have the user on our db, but ejabberd could have it, so we
    # query and cache the result
    async with app.ejabberd.check_account(jid) as resp:
        if resp.status != 200:
            raise Exception(f"Expected 200, got {resp.status}")

        rjson = await resp.json()
        if rjson != 0:
            app.ejabberd.unknown_jid_cache[jid] = True
            raise Unauthorized("Account not found")

        _ = await User.create(jid)
        return jsonify({"authentication_methods": {"xmpp": True, "email": False}})


@bp.route("/login/start", methods=["POST"])
async def start_login():
    j = validate(await request.get_json(), LOGIN_START_SCHEMA)
    jid = j["jid"]
    auth_method: AuthMethod = j["auth_method"]

    user = await User.fetch(jid)
    if user is None:
        raise Unauthorized("Account not found")

    if auth_method == AuthMethod.email and user.email is None:
        raise BadRequest("A linked email is required to be able to login via email")

    token = await Token.create(jid, TokenType.Login)

    if auth_method == AuthMethod.xmpp:
        async with app.ejabberd.send_message(
            user.jid, f"Login Token: {token.value}"
        ) as resp:
            assert resp.status == 200
            rjson = await resp.json()
            if rjson != 0:
                raise Exception("Failed to send message to XMPP")
    elif auth_method == AuthMethod.email:
        assert user.email is not None
        job_id = await EmailQueue.submit(
            user.email,
            "awoo!!! - login",
            f"here's the token: '{token.value}'\nTODO: url maybe?",
        )

        return jsonify({"job_id": str(job_id)})

    return "", 204


@bp.route("/login/finish", methods=["POST"])
async def finish_login():
    j = validate(await request.get_json(), LOGIN_FINISH_SCHEMA)
    token_value = j["token"]

    token = await Token.fetch(TokenType.Login, token_value)
    # TODO: if otp is invalid, bump retries on user table
    # TODO: if retries >= 3, invalidate otp, set retries to 0
    # TODO on three invalid tries for given jid, invalidate token, add
    # a 'retries' key to payload body
    # TODO: return number of retries on failure
    if token is None:
        raise Unauthorized("Invalid token")

    jid = token.jid
    user = await User.fetch(jid)
    assert user is not None

    # TODO: if otp is valid, set retries to 0

    bearer = await Token.create(jid, TokenType.Bearer)

    # after successful bearer token generation, delete old token
    await token.delete()
    return jsonify({"user": user.to_dict(), "token": bearer.value})
