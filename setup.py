# lasauthgna: user management on top of ejabberd
# Copyright 2020, LavaTech and the lasauthgna contributors
# SPDX-License-Identifier: AGPL-3.0-only

from setuptools import setup

setup(
    name="lasauthgna",
    version="0.1.0",
    description="user management system on top of ejabberd",
    # TODO a website, maybe.
    url="https://example.test",
    author="LavaTech",
    python_requires=">=3.8",
    install_requires=[
        # hypercorn and quart should be updated atomically
        "hypercorn==0.13.2",
        "Quart==0.17.0",
        "itsdangerous==2.1.2",
        "cerberus==1.3.4",
        "asyncpg==0.25.0",
        "tomlkit==0.10.2",
        "aiohttp==3.8.1",
        "expiringdict==1.2.1",
        "violet @ git+https://gitlab.com/elixire/violet.git@ea5b8373c46dc8f5314ef44f2570c00059d58d3b#egg=violet",
        "hail @ git+https://gitlab.com/elixire/hail.git@d481786d256e682f6992ca250e9f8516205d0608#egg=hail",
    ],
    entry_points="""
    [console_scripts]
    lauth=cli:main
    """,
)
