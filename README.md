# lasauthgna

account management system on top of ejabberd for a3.pm (wip)

## Installing

- Python 3.8
- PostgreSQL

It is recommended to setup a virtual environemnt to develop.
It is also recommended to use a virtualenv helper to develop,
such as [virtualfish] or [virtualenvwrapper].

[virtualfish]: https://github.com/justinmayer/virtualfish
[virtualenvwrapper]: https://bitbucket.org/dhellmann/virtualenvwrapper

```
python3 -m pip install --editable .

# specifics of creating the database and user for it are deploy-specific.
# also see the 'Deploying' section for ejabberd.
cp config.example.toml config.toml

# arguments may be different to load the schema
psql -U lasauthgna -f schema.sql

hypercorn lasauthgna --access-log -
```

## Deploying

For the `lauth` cli tool to work, your XMPP server address MUST provide
a JSON document on the `/.well-known/xmpp-auth.json` path (MUST be an https-reachable resource).
For example, `https://a3.pm/.well-known/xmpp-auth.json` would return the following document:

```
{
  "links": [
    {
      "rel": "urn:xmpp-auth:lasauthgna",
      "href": "https://auth.a3.pm"
    }
  ]
}
```

### NGINX

We use NGINX in production to handle `.well-known`. Here's what you can do for it:

```
location /.well-known/xmpp-auth.json {
    default_type 'application/jrd+json';
    return 200 '{"links":[{"rel":"urn:xmpp-auth:lasauthgna","href":"https://auth.a3.pm"}]}';
}
```

### ejabberd

For ejabberd, the lasauthgna user SHOULD have specific API permissions instead
of a full admin user.

For a3.pm's deployment, see https://gitlab.com/lavatech/a3.pm/-/commit/c113f5cf012d2d51b2d936fda470dc47a6186830

```
acl:
  lasauthgna:
    user:
      - "lasauthgna@my_server.test"

# [...at a later section...]

api_permissions:
  "lasauthgna administration":
    who:
      - access:
          - allow:
              - acl: lasauthgna
    what:
      - "send_message"
      - "change_password"
      - "check_account"
```

## Development

### Running the test suite

- Have [tox] installed

[tox]: https://tox.readthedocs.io/en/latest/

```
tox
```

### Testing manually

You can test with the following setup:

- A local NGINX instance containing the `/.well-known/xmpp-auth.json` entry
  pointing to `localhost:some_local_address`
- A local ejabberd instance with ports properly configured on your `config.toml`

The `lauth` CLI utility supports using `localhost`, for example:

```
lauth --force-http mytestuser@localhost login
```

### Creating a database migration

**TODO: finish move away from agnostic**
